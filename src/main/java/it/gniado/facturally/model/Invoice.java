package it.gniado.facturally.model;

import java.time.LocalDate;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotNull;

@Entity
public class Invoice extends AbstractModel {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@ManyToOne
	private User issuer;

	@NotNull
	@ManyToOne
	private Contractor contractor;

	@NotNull
	private LocalDate issueDate;

	@NotNull
	private LocalDate sellingDate;

	@NotNull
	private LocalDate paymentDate;

	public Invoice() {
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public User getIssuer() {
		return issuer;
	}

	public void setIssuer(User issuer) {
		this.issuer = issuer;
	}

	public Contractor getContractor() {
		return contractor;
	}

	public void setContractor(Contractor contractor) {
		this.contractor = contractor;
	}

	public LocalDate getIssueDate() {
		return issueDate;
	}

	public void setIssueDate(LocalDate issueDate) {
		this.issueDate = issueDate;
	}

	public LocalDate getSellingDate() {
		return sellingDate;
	}

	public void setSellingDate(LocalDate sellingDate) {
		this.sellingDate = sellingDate;
	}

	public LocalDate getPaymentDate() {
		return paymentDate;
	}

	public void setPaymentDate(LocalDate paymentDate) {
		this.paymentDate = paymentDate;
	}
}
