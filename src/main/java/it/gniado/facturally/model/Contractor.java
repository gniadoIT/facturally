package it.gniado.facturally.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

@Entity
public class Contractor extends AbstractModel {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String name;

	@OneToOne(fetch = FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	private Address address;

	private String bankName;
	private String bankAccount;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false, unique = true)
	private String identityNumber;

	public Contractor() {
	}

	public Contractor(Long id, String name, Address address, String bankAccount, String identityNumber) {
		this.id = id;
		this.name = name;
		this.address = address;
		this.bankAccount = bankAccount;
		this.identityNumber = identityNumber;
	}

	@Override
	public Long getId() {

		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	public String getBankAccount() {
		return bankAccount;
	}

	public void setBankAccount(String bankAccount) {
		this.bankAccount = bankAccount;
	}

	public String getIdentityNumber() {
		return identityNumber;
	}

	public void setIdentityNumber(String identityNumber) {
		this.identityNumber = identityNumber;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof Contractor))
			return false;

		Contractor that = (Contractor) o;

		return new EqualsBuilder().append(id, that.id).append(name, that.name).append(address.getId(), that.address
				.getId())
				.append(bankAccount, that.bankAccount).append(identityNumber, that.identityNumber).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(name).append(address.getId()).append(bankAccount)
				.append(identityNumber).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("name", name).append("address", address)
				.append("bankAccount", bankAccount).append("identityNumber", identityNumber).toString();
	}
}
