package it.gniado.facturally.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;
import org.hibernate.annotations.Cascade;
import org.hibernate.annotations.CascadeType;

import it.gniado.facturally.annotations.NotEmpty;

@Entity
@Table
public class User extends AbstractModel {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String firstName;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String lastName;

	@NotNull
	@NotEmpty
	@Column(nullable = false, unique = true)
	private String login;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String password;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String companyName;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String identifyNumber;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String bankName;

	@NotNull
	@NotEmpty
	@Column(nullable = false)
	private String bankAccountNumber;

	@OneToOne(fetch = FetchType.EAGER)
	@Cascade(CascadeType.ALL)
	private Address address;

	public User() {
	}

	public User(String login, String password, String firstName, String lastName, String companyName, String
			identifyNumber, String bankName, String bankAccountNumber, Address address) {
		this.login = login;
		this.password = password;
		this.firstName = firstName;
		this.lastName = lastName;
		this.companyName = companyName;
		this.identifyNumber = identifyNumber;
		this.bankName = bankName;
		this.bankAccountNumber = bankAccountNumber;
		this.address = address;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getCompanyName() {
		return companyName;
	}

	public void setCompanyName(String companyName) {
		this.companyName = companyName;
	}

	public String getIdentifyNumber() {
		return identifyNumber;
	}

	public void setIdentifyNumber(String identifyNumber) {
		this.identifyNumber = identifyNumber;
	}

	public String getBankName() {
		return bankName;
	}

	public void setBankName(String bankName) {
		this.bankName = bankName;
	}

	public String getBankAccountNumber() {
		return bankAccountNumber;
	}

	public void setBankAccountNumber(String bankAccountNumber) {
		this.bankAccountNumber = bankAccountNumber;
	}

	public Address getAddress() {
		return address;
	}

	public void setAddress(Address address) {
		this.address = address;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof User))
			return false;

		User user = (User) o;

		return new EqualsBuilder().append(id, user.id).append(login, user.login).append(password, user.password)
				.append(companyName, user.companyName).append(identifyNumber, user.identifyNumber)
				.append(bankName, user.bankName).append(bankAccountNumber, user.bankAccountNumber)
				.append(address, user.address).isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(login).append(password).append(companyName)
				.append(identifyNumber).append(bankName).append(bankAccountNumber).append(address).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("login", login).append("password", password)
				.append("companyName", companyName).append("identifyNumber", identifyNumber)
				.append("bankName", bankName).append("bankAccountNumber", bankAccountNumber).append("address", address)
				.toString();
	}
}
