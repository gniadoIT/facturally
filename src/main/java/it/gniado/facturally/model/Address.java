package it.gniado.facturally.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.apache.commons.lang3.builder.EqualsBuilder;
import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.apache.commons.lang3.builder.ToStringBuilder;

@Entity
public class Address extends AbstractModel {

	@Id
	@GeneratedValue
	private Long id;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String street;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String houseNumber;
	private String apartmentNumber;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String postalCode;

	@NotNull
	@Size(min = 1)
	@Column(nullable = false)
	private String city;
	private String postCity;

	public Address() {
	}

	public Address(String street, String houseNumber, String apartmentNumber, String postalCode, String city,
			String postCity) {
		this.street = street;
		this.houseNumber = houseNumber;
		this.apartmentNumber = apartmentNumber;
		this.postalCode = postalCode;
		this.city = city;
		this.postCity = postCity;
	}

	@Override
	public Long getId() {
		return id;
	}

	@Override
	public void setId(Long id) {
		this.id = id;
	}

	public String getStreet() {
		return street;
	}

	public void setStreet(String street) {
		this.street = street;
	}

	public String getHouseNumber() {
		return houseNumber;
	}

	public void setHouseNumber(String houseNumber) {
		this.houseNumber = houseNumber;
	}

	public String getApartmentNumber() {
		return apartmentNumber;
	}

	public void setApartmentNumber(String apartmentNumber) {
		this.apartmentNumber = apartmentNumber;
	}

	public String getPostalCode() {
		return postalCode;
	}

	public void setPostalCode(String postalCode) {
		this.postalCode = postalCode;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getPostCity() {
		return postCity;
	}

	public void setPostCity(String postCity) {
		this.postCity = postCity;
	}

	@Override
	public boolean equals(Object o) {
		if (this == o)
			return true;

		if (!(o instanceof Address))
			return false;

		Address address = (Address) o;

		return new EqualsBuilder().append(id, address.id).append(street, address.street)
				.append(houseNumber, address.houseNumber).append(apartmentNumber, address.apartmentNumber)
				.append(postalCode, address.postalCode).append(city, address.city).append(postCity, address.postCity)
				.isEquals();
	}

	@Override
	public int hashCode() {
		return new HashCodeBuilder(17, 37).append(id).append(street).append(houseNumber).append(apartmentNumber)
				.append(postalCode).append(city).append(postCity).toHashCode();
	}

	@Override
	public String toString() {
		return new ToStringBuilder(this).append("id", id).append("street", street).append("houseNumber", houseNumber)
				.append("apartmentNumber", apartmentNumber).append("postalCode", postalCode).append("city", city)
				.append("postCity", postCity).toString();
	}
}
