package it.gniado.facturally.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import it.gniado.facturally.dao.interfaces.UserDao;
import it.gniado.facturally.model.User;

@Stateless
public class UserDaoImpl extends AbstractDaoImpl<User> implements UserDao {

	private static final String FIND_ALL_USERS = "SELECT u FROM User u";
	private static final String FIND_USER_BY_ID = "SELECT u FROM User u WHERE u.id = :id";
	private static final String FIND_BY_LOGIN_AND_PASSWORD =
			"SELECT u FROM User u WHERE u.login = :login AND u" + ".password = :password";
	private static final String FIND_BY_LOGIN = "SELECT u FROM User u WHERE u.login = :login";
	private static final String DELETE_BY_ID = "DELETE FROM User u WHERE u.id = :id";

	@Override
	public List<User> findAll() {
		TypedQuery<User> query = em.createQuery(FIND_ALL_USERS, User.class);
		return query.getResultList();
	}

	@Override
	public User verifyByLoginAndPassword(String login, String password) {
		TypedQuery<User> query = em.createQuery(FIND_BY_LOGIN_AND_PASSWORD, User.class);
		query.setParameter("login", login);
		query.setParameter("password", password);
		return query.getSingleResult();
	}

	@Override
	public User findById(Long id) {
		TypedQuery<User> query = em.createQuery(FIND_USER_BY_ID, User.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public User findByLogin(String login) {
		TypedQuery<User> query = em.createQuery(FIND_BY_LOGIN, User.class);
		query.setParameter("login", login);
		return query.getSingleResult();
	}

	@Override
	public void delete(Long id) {
		Query query = em.createQuery(DELETE_BY_ID);
		query.setParameter("id", id);
		query.executeUpdate();
	}
}
