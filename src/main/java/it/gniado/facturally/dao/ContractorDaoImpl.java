package it.gniado.facturally.dao;

import java.util.List;

import javax.ejb.Stateless;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import it.gniado.facturally.dao.interfaces.ContractorDao;
import it.gniado.facturally.model.Contractor;

@Stateless
public class ContractorDaoImpl extends AbstractDaoImpl<Contractor> implements ContractorDao {

	private static final String FIND_ALL_CONTRACTORS = "SELECT c FROM Contractor c";
	private static final String FIND_CONTRACTOR_BY_ID = "SELECT c FROM Contractor c WHERE c.id = :id";
	private static final String DELETE_CONTRACTOR_BY_ID = "DELETE FROM Contractor c WHERE c.id = :id";

	@Override
	public List<Contractor> findAll() {
		TypedQuery<Contractor> query = em.createQuery(FIND_ALL_CONTRACTORS, Contractor.class);
		return query.getResultList();
	}

	@Override
	public Contractor findById(Long id) {
		TypedQuery<Contractor> query = em.createQuery(FIND_CONTRACTOR_BY_ID, Contractor.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public void delete(Long id) {
		Query query = em.createQuery(DELETE_CONTRACTOR_BY_ID);
		query.setParameter("id", id);
		query.executeUpdate();
	}
}
