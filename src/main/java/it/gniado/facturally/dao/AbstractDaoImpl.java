package it.gniado.facturally.dao;

import java.util.List;

import javax.ejb.Local;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import it.gniado.facturally.dao.interfaces.AbstractDao;
import it.gniado.facturally.model.AbstractModel;

@Local
public class AbstractDaoImpl<T extends AbstractModel> implements AbstractDao<T> {

	@PersistenceContext(name = "FacturallyDS")
	protected EntityManager em;

	@Override
	public Long persist(T entity) {
		em.persist(entity);
		return entity.getId();
	}

	@Override
	public void delete(T entity) {
		em.remove(entity);
	}

	@Override
	public List<T> findAll() {
		return null;
	}

	@Override
	public T update(T entity) {
		return em.merge(entity);
	}
}
