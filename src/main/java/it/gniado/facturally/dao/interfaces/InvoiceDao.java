package it.gniado.facturally.dao.interfaces;

import java.util.List;

import it.gniado.facturally.model.Invoice;

public interface InvoiceDao extends AbstractDao<Invoice> {

	Invoice findById(Long id);
	List<Invoice> findAllUsersInvoices(Long userId);
	List<Invoice> findAllInvoicesForContractor(Long contractorId);
	List<Invoice> findAllUsersContractorInvoices(Long userId, Long contractorId);

}
