package it.gniado.facturally.dao.interfaces;

import javax.ejb.Local;

import it.gniado.facturally.model.User;

@Local
public interface UserDao extends AbstractDao<User> {

	User verifyByLoginAndPassword(String login, String password);
	User findById(Long id);
	User findByLogin(String login);
	void delete(Long id);

}
