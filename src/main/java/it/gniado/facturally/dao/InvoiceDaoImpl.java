package it.gniado.facturally.dao;

import java.util.List;

import javax.persistence.TypedQuery;

import it.gniado.facturally.dao.interfaces.InvoiceDao;
import it.gniado.facturally.model.Invoice;

public class InvoiceDaoImpl extends AbstractDaoImpl<Invoice> implements InvoiceDao {

	private static final String FIND_BY_ID = "SELECT i FROM Invoice i WHERE i.id = :id";
	private static final String FIND_ALL_USERS_INVOICES = "SELECT i FROM Invoice i JOIN User u WHERE u.id = :userId";
	private static final String FIND_ALL_FOR_CONTRACTOR = "SELECT i FROM Invoice i JOIN Contractor c WHERE c.id = " +
														  ":contractorId";
	private static final String FIND_ALL_USER_CONTRACTOR_INVOICES = "SELECT i FROM Invoice i JOIN User u, Contractor c " +
																	"WHERE c.id = :contractorId AND u.id = :userId";
	private static final String FIND_ALL = "SELECT i FROM Invoice i";

	@Override
	public Invoice findById(Long id) {
		TypedQuery<Invoice> query = em.createQuery(FIND_BY_ID, Invoice.class);
		query.setParameter("id", id);
		return query.getSingleResult();
	}

	@Override
	public List<Invoice> findAllUsersInvoices(Long userId) {
		TypedQuery<Invoice> query = em.createQuery(FIND_ALL_USERS_INVOICES, Invoice.class);
		query.setParameter("userId", userId);
		return query.getResultList();
	}

	@Override
	public List<Invoice> findAllInvoicesForContractor(Long contractorId) {
		TypedQuery<Invoice> query = em.createQuery(FIND_ALL_FOR_CONTRACTOR, Invoice.class);
		query.setParameter("contractorId", contractorId);
		return query.getResultList();
	}

	@Override
	public List<Invoice> findAllUsersContractorInvoices(Long userId, Long contractorId) {
		TypedQuery<Invoice> query = em.createQuery(FIND_ALL_USER_CONTRACTOR_INVOICES, Invoice.class);
		query.setParameter("userId", userId);
		query.setParameter("contractorId", contractorId);
		return query.getResultList();
	}

	@Override
	public List<Invoice> findAll() {
		TypedQuery<Invoice> query = em.createQuery(FIND_ALL, Invoice.class);
		return query.getResultList();
	}
}
