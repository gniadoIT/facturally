package it.gniado.facturally.annotations;


import java.lang.annotation.Documented;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import it.gniado.facturally.annotations.impl.NotEmptyImpl;

import static java.lang.annotation.ElementType.FIELD;
import static java.lang.annotation.ElementType.PARAMETER;
import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = NotEmptyImpl.class)
@Target({ FIELD, PARAMETER})
@Retention(RUNTIME)
@Documented
public @interface NotEmpty {

	String message() default "{Value cannot be empty}";

	Class<?>[] groups() default {};

	Class<? extends Payload>[] payload() default {};

}
