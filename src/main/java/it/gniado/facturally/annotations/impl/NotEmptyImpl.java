package it.gniado.facturally.annotations.impl;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import it.gniado.facturally.annotations.NotEmpty;

public class NotEmptyImpl implements ConstraintValidator<NotEmpty, String> {


	@Override
	public void initialize(NotEmpty notEmpty) {
	}

	@Override
	public boolean isValid(String value, ConstraintValidatorContext constraintValidatorContext) {
		if (value == null) {
			return true;
		}

		if (value.length() < 1) {
			return false;
		}

		return true;
	}
}
