package it.gniado.facturally.service.interfaces;

import javax.validation.Valid;

import it.gniado.facturally.model.User;

public interface RegistrationService {

	void register(@Valid User user);

}
