package it.gniado.facturally.service;

import javax.ejb.EJB;
import javax.enterprise.context.RequestScoped;
import javax.validation.Valid;

import it.gniado.facturally.dao.interfaces.UserDao;
import it.gniado.facturally.model.User;
import it.gniado.facturally.service.interfaces.RegistrationService;

@RequestScoped
public class RegistrationServiceImpl implements RegistrationService {

	@EJB
	private UserDao dao;

	@Override
	public void register(@Valid User user) {
		dao.persist(user);
	}
}
