package it.gniado.facturally.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.apache.commons.lang3.StringUtils;
import static org.fest.assertions.Assertions.*;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import it.gniado.facturally.model.Address;
import it.gniado.facturally.model.Contractor;
import it.gniado.facturally.PersistenceConfiguration;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceConfiguration.class, ContractorDaoImpl.class })
@Transactional
public class ContractorDaoImplTest{

	@PersistenceContext
	private EntityManager em;

	@Inject
	private ContractorDaoImpl dao;

	@Test
	public void testPersist() {
		// GIVEN
		Contractor contractor = createContractor();

		// WHEN
		Long id = dao.persist(contractor);

		// THEN no exception is thrown
		assertThat(id).isNotNull();
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_constraintViolation(){
		//GIVEN
		Contractor contractor = new Contractor();
		contractor.setName("Contractor");
		contractor.setAddress(createAddress());

		// WHEN
		dao.persist(contractor);
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_relatedViolatedNotNull(){
		//GIVEN
		Contractor contractor = new Contractor();
		Address address = createAddress();
		address.setCity(null);
		contractor.setAddress(address);

		// WHEN
		dao.persist(contractor);
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_relatedViolatedNotEmpty(){
		//GIVEN
		Contractor contractor = new Contractor();
		Address address = createAddress();
		address.setCity(StringUtils.EMPTY);
		contractor.setAddress(address);

		// WHEN
		dao.persist(contractor);
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_uniqueViolation(){
		// GIVEN
		Contractor contractor = new Contractor();
		Contractor violatingContractor = new Contractor();
		dao.persist(contractor);

		// WHEN
		dao.persist(violatingContractor);
	}

	@Test(expected = NoResultException.class)
	public void testDelete() {
		// GIVEN
		Contractor contractor = createContractor();
		Long id = dao.persist(contractor);

		// WHEN
		dao.delete(contractor);

		// THEN
		dao.findById(id);
	}

	@Test(expected = NoResultException.class)
	public void testDelete_byId(){
		// GIVEN
		Contractor contractor = createContractor();
		Long id = dao.persist(contractor);

		// WHEN
		dao.delete(id);

		// THEN
		dao.findById(id);
	}

	@Test
	public void testRead() {
		// GIVEN
		Contractor contractor = createContractor();
		Long id = dao.persist(contractor);

		// WHEN
		Contractor persisted = dao.findById(id);

		// THEN
		assertThat(persisted).isEqualTo(contractor);
	}

	@Test(expected = NoResultException.class)
	public void testRead_unpersisted() {
		// GIVEN NO DATA

		// WHEN
		dao.findById(1L);
	}

	@Test
	public void testUpdate() {
		// GIVEN
		Contractor contractor = createContractor();
		dao.persist(contractor);
		em.detach(contractor);
		Contractor toUpdate = createContractor();
		toUpdate.setId(contractor.getId());
		toUpdate.setName("Changed Name");
		toUpdate.setAddress(contractor.getAddress());

		// WHEN
		Contractor updated = dao.update(toUpdate);

		// THEN
		assertThat(updated).isEqualTo(toUpdate);
		assertThat(updated).isNotEqualTo(contractor);
	}

	private Contractor createContractor() {
		Address address = createAddress();
		Contractor contractor = new Contractor();
		contractor.setName("Gniado IT - Bartosz Gniado");
		contractor.setBankAccount("11 2222 3333 4444 5555 6666");
		contractor.setIdentityNumber("5783017082");
		contractor.setAddress(address);
		return contractor;
	}

	private Address createAddress() {
		Address address = new Address();
		address.setStreet("Słoneczna");
		address.setHouseNumber("40");
		address.setApartmentNumber("13");
		address.setPostalCode("82-300");
		address.setCity("Elbląg");
		return address;
	}
}