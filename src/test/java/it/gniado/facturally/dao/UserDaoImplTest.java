package it.gniado.facturally.dao;

import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.PersistenceException;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.transaction.annotation.Transactional;

import it.gniado.facturally.PersistenceConfiguration;
import it.gniado.facturally.model.Address;
import it.gniado.facturally.model.User;

import static org.fest.assertions.Assertions.assertThat;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = { PersistenceConfiguration.class, UserDaoImpl.class })
@Transactional
public class UserDaoImplTest {

	public static final String CHANGED_NAME = "CHANGED NAME";
	@PersistenceContext
	private EntityManager em;

	@Inject
	private UserDaoImpl dao;

	@Test
	public void testPersist() {
		// GIVEN
		User user = createUser();

		// WHEN
		dao.persist(user);

		// THEN
		assertThat(user.getId()).isNotNull();
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_constraintViolation() {
		// GIVEN
		User user = new User();

		// WHEN
		dao.persist(user);
	}

	@Test(expected = PersistenceException.class)
	public void testPersist_uniqueViolation() {
		// GIVEN
		User user = createUser();
		dao.persist(user);
		User violating = createUser();

		// WHEN
		dao.persist(violating);
	}

	@Test(expected = NoResultException.class)
	public void testDelete() {
		// GIVEN
		User user = createUser();
		dao.persist(user);

		// WHEN
		dao.delete(user);

		// THEN
		dao.findById(user.getId());
	}

	@Test(expected = NoResultException.class)
	public void testDelete_byId() {
		// GIVEN
		User user = createUser();
		dao.persist(user);

		// WHEN
		dao.delete(user.getId());

		// THEN
		dao.findById(user.getId());
	}

	@Test
	public void testRead() {
		// GIVEN
		User user = createUser();
		Long id = dao.persist(user);

		// WHEN
		User persisted = dao.findById(id);

		// THEN
		assertThat(persisted).isEqualTo(user);
	}

	@Test(expected = NoResultException.class)
	public void testRead_unpersisted() {
		// GIVEN NO DATA

		// WHEN
		dao.findById(1L);
	}

	@Test
	public void testUpdate() {
		// GIVEN
		User user = createUser();
		dao.persist(user);
		User toUpdate = createUser();
		toUpdate.setId(user.getId());
		toUpdate.setCompanyName(CHANGED_NAME);

		// WHEN
		dao.update(toUpdate);

		// THEN
		User updated = dao.findById(user.getId());
		assertThat(updated.getId()).isEqualTo(user.getId());
		assertThat(updated.getCompanyName()).isEqualTo(CHANGED_NAME);
	}

	private User createUser() {
		User user = new User();
		user.setFirstName("Bartosz");
		user.setLastName("Gniado");
		user.setCompanyName("Gniado IT - Bartosz Gniado");
		user.setIdentifyNumber("5783017082");
		user.setBankAccountNumber("11 2222 3333 4444 5555 6666");
		user.setBankName("IDEA Bank");
		user.setLogin("gniadek");
		user.setPassword("hasło");
		return user;
	}

	private Address createAddress() {
		Address address = new Address();
		address.setStreet("Słoneczna");
		address.setHouseNumber("40");
		address.setApartmentNumber("13");
		address.setPostalCode("82-300");
		address.setCity("Elbląg");
		return address;
	}
}
