package it.gniado.facturally;

import java.util.Properties;

import javax.sql.DataSource;

import org.h2.Driver;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.SimpleDriverDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.persistenceunit.MutablePersistenceUnitInfo;
import org.springframework.orm.jpa.persistenceunit.PersistenceUnitPostProcessor;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;

@Configuration
public class PersistenceConfiguration {

	@Bean
	public DataSource dataSource() {

		SimpleDriverDataSource dataSource = new SimpleDriverDataSource();
		dataSource.setDriverClass(Driver.class);
		dataSource.setUrl("jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1;DB_CLOSE_ON_EXIT=false;");
		dataSource.setUsername("sa");
		dataSource.setPassword("");

		return dataSource;
	}

	@Bean
	public JpaTransactionManager transactionManager() throws Exception {
		return new JpaTransactionManager(entityManagerFactory().getObject());
	}

	@Bean
	public LocalContainerEntityManagerFactoryBean entityManagerFactory() throws Exception {
		LocalContainerEntityManagerFactoryBean bean = new LocalContainerEntityManagerFactoryBean();
		bean.setDataSource(dataSource());
		HibernateJpaVendorAdapter hibernateAdapter = new HibernateJpaVendorAdapter();
		hibernateAdapter.setShowSql(true);
		hibernateAdapter.setGenerateDdl(true);
		bean.setJpaVendorAdapter(hibernateAdapter);
		bean.setPersistenceUnitName("FacturallyDS");
		bean.setPersistenceXmlLocation("/persistence.xml");
		PersistenceUnitPostProcessor postProcessor = new PersistenceUnitPostProcessor() {

			public void postProcessPersistenceUnitInfo(MutablePersistenceUnitInfo pui) {
				pui.setJtaDataSource(null);
				Properties properties = pui.getProperties();
				properties.setProperty("hibernate.cache.use_query_cache", "false");
			}
		};
		bean.setPersistenceUnitPostProcessors(postProcessor);
		return bean;
	}

}
